package com.leyou.cart.service;

import com.leyou.auth.entiy.UserInfo;
import com.leyou.cart.client.GoodsClient;
import com.leyou.cart.filter.LoginInterceptor;
import com.leyou.cart.pojo.Cart;
import com.leyou.common.enums.ExceptionEnums;
import com.leyou.common.exception.LyException;
import com.leyou.common.utils.JsonUtils;
import com.leyou.item.pojo.Sku;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService{
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private GoodsClient goodsClient;

    static final String KEY_PREFIX = "ly:cart:uid:";

    static final Logger logger = LoggerFactory.getLogger(CartService.class);

    @Override
    public void addCart(Cart cart) {
        //获取登录对象
        UserInfo user = LoginInterceptor.getLoginUser();
        //redis的key
        String key = KEY_PREFIX+user.getId();
        //获取hash操作对象
        BoundHashOperations<String,Object,Object> hashOps = redisTemplate.boundHashOps(key);
        //查询是否存在
        Long skuId = cart.getSkuId();
        Integer num = cart.getNum();
        Boolean boo = hashOps.hasKey(skuId.toString());
        //1.存在
        if(boo){
            //存在则获取购物车
            String cartShop = hashOps.get(skuId.toString()).toString();
            cart = JsonUtils.toBean(cartShop,Cart.class);
            //添加数量
            cart.setNum(cart.getNum()+num);
        }else {
            //2.不存在，添加购物车数据
            cart.setUserId(user.getId());
            //查询商品sku信息
            Sku sku = goodsClient.querySkuById(skuId);
            if(sku==null){
                logger.error("查询的商品不存在：skuid：{}",skuId);
                throw new LyException(ExceptionEnums.SKU_NOT_FOUND);
            }
            //设置cart属性  图片，价格，标题，单独的属性
            cart.setImage(StringUtils.isNotBlank(sku.getImages())?"":StringUtils.split(sku.getImages(),",")[0]);
            cart.setPrice(sku.getPrice());
            cart.setTitle(sku.getTitle());
            cart.setOwnSpec(sku.getOwnSpec());
        }
        //写入redis
        hashOps.put(cart.getSkuId().toString(),JsonUtils.toString(cart));

    }

    /**
     * 查询购物车
     * @return
     */
    @Override
    public List<Cart> queryCartList() {
        //先获取登录用户
        UserInfo userInfo = LoginInterceptor.getLoginUser();
        //判断是否存在购物车
        String key = KEY_PREFIX+userInfo.getId();
        if(!redisTemplate.hasKey(key)){
            return null;
        }
        //判断数据是否存在
        BoundHashOperations<String,Object,Object> hashOps = redisTemplate.boundHashOps(key);
        List<Object> carts = hashOps.values();

        if(CollectionUtils.isEmpty(carts)){
            return null;
        }
        //查询购物车数据
        List<Cart> cartList = carts.stream().map(o->
                JsonUtils.toBean(o.toString(),Cart.class)).collect(Collectors.toList());
        return cartList;
    }

    /**
     * 修改购物车的数量
     * @param skuId
     * @param num
     */
    @Override
    public void updateNum(Long skuId, Integer num) {
        //获取用户
        UserInfo userInfo = LoginInterceptor.getLoginUser();
        //获取购物车并修改数量
        String key = KEY_PREFIX+userInfo.getId();
        BoundHashOperations<String,Object,Object> hashOps = redisTemplate.boundHashOps(key);
        String json = hashOps.get(skuId.toString()).toString();
        Cart cart = JsonUtils.toBean(json, Cart.class);
        cart.setNum(num);
        //写入购物车
        hashOps.put(skuId.toString(),JsonUtils.toString(cart));
    }

    /**
     * 删除购物车sku商品
     * @param skuId
     */
    @Override
    public void deleteCartBySkuId(Long skuId) {
        //先获取登录用户
        UserInfo userInfo = LoginInterceptor.getLoginUser();
        //判断是否存在购物车
        String key = KEY_PREFIX+userInfo.getId();
        if(!redisTemplate.hasKey(key)){
            throw new LyException(ExceptionEnums.CART_NOT_SKU);
        }
        //删除购物车
        BoundHashOperations<String,Object,Object> hashOps = redisTemplate.boundHashOps(key);
        hashOps.delete(key);
    }
}
