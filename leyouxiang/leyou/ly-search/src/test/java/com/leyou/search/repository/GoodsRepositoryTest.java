package com.leyou.search.repository;

import com.leyou.common.vo.PageResult;
import com.leyou.item.vo.SpuVo;
import com.leyou.search.feignclient.GoodsClient;
import com.leyou.search.pojo.Goods;
import com.leyou.search.service.SearchService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GoodsRepositoryTest {
    @Autowired
    ElasticsearchTemplate template;
    @Autowired
    GoodsRepository goodsRepository;
    @Autowired
    GoodsClient goodsClient;
    @Autowired
    SearchService searchService;

    @Test
    public void creatIndex(){
        template.createIndex(Goods.class);
        template.putMapping(Goods.class);
    }
    @Test
    public void deleteIndex(){
        template.deleteIndex(Goods.class);
    }
    @Test
    public void loadData(){
        int page = 1;
        int rows = 100;
        int size = 0;
        do {
            //查询spu
            PageResult<SpuVo> spuVoPageResult = goodsClient.querySpuPage(page, rows, true, null);
            List<SpuVo> spuVos = spuVoPageResult.getItems();
            if(CollectionUtils.isEmpty(spuVos)){
                break;
            }
            //构建成goods
            List<Goods> goods = spuVos.stream().map(searchService::buildsGoods).collect(Collectors.toList());
            //存入
            goodsRepository.saveAll(goods);

            size = spuVos.size();

            page++;
        }while (size==100);

    }
}