package com.leyou.search.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.leyou.common.enums.ExceptionEnums;
import com.leyou.common.exception.LyException;
import com.leyou.common.utils.JsonUtils;
import com.leyou.common.vo.PageResult;
import com.leyou.item.pojo.*;
import com.leyou.search.feignclient.BrandClient;
import com.leyou.search.feignclient.CategoryClient;
import com.leyou.search.feignclient.GoodsClient;
import com.leyou.search.feignclient.SpecClient;
import com.leyou.search.pojo.Goods;
import com.leyou.search.pojo.SearchRequest;
import com.leyou.search.pojo.SearchResult;
import com.leyou.search.repository.GoodsRepository;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.LongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.FetchSourceFilter;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SearchServiceImpl implements SearchService{
    @Autowired
    private CategoryClient categoryClient;
    @Autowired
    private BrandClient brandClient;
    @Autowired
    private GoodsClient goodsClient;
    @Autowired
    private GoodsRepository goodsRepository;

    @Autowired
    private SpecClient specClient;

    @Autowired
    private ElasticsearchTemplate template;

    private static final Logger logger = LoggerFactory.getLogger(SearchService.class);

    /**
     * 搜索商品
     * @param request
     * @return
     */
    @Override
    public PageResult<Goods> search(SearchRequest request) {
        String key = request.getKey();
        if(StringUtils.isBlank(key)){
            return null;
        }
        //分页参数
        int page = request.getPage()-1;
        int size = request.getSize();
        //构建查询条件
        NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
        //过滤  fetchSourceFilter 拉取资源过滤
        queryBuilder.withSourceFilter(new FetchSourceFilter(new String[]{"id","skus","subTitle"},null));
        //添加查询条件--分页
        queryBuilder.withPageable(PageRequest.of(page,size));
        //基本查询
       // QueryBuilder basicQuery = QueryBuilders.matchQuery("all", key);
        QueryBuilder basicQuery = buildBasicQueryWithFilter(request);
        queryBuilder.withQuery(basicQuery);
        //分页排序
        searchWithPageAndSort(queryBuilder,request);

        //添加聚合
        //聚合搜索条件  商品
        String categoryAggName = "category";
        queryBuilder.addAggregation(AggregationBuilders.terms(categoryAggName).field("cid3"));
        //品牌
        String brandAggName = "brand";
        queryBuilder.addAggregation(AggregationBuilders.terms(brandAggName).field("brandId"));

        //查询聚合
        AggregatedPage<Goods> pageInfo = template.queryForPage(queryBuilder.build(),Goods.class);

        //解析聚合结果
        long total = pageInfo.getTotalElements();
        long totalPage = (total+request.getSize()-1)/request.getSize();


        Aggregations aggs = pageInfo.getAggregations();
        //商品聚合
        List<Category> categories = getCategoryAggResult(aggs.get(categoryAggName));
        //品牌聚合
        List<Brand> brands = getBrandAggResult(aggs.get(brandAggName));


        //判断商品分类聚合结果是否等于1，等于则聚合其他参数
        List<Map<String,Object>> specs = null;

        if(categories.size()==1){
            specs = getSpecs(categories.get(0),basicQuery);
        }


        return new SearchResult(total,totalPage,pageInfo.getContent(),categories,brands,specs);
    }

    /**
     * 新增商品索引
     * @param id
     */
    @Override
    public void createIndex(Long id) {
        Spu spu = goodsClient.querySpuById(id);
        Goods goods = buildsGoods(spu);
        goodsRepository.save(goods);
    }



    @Override
    public void deleteIndex(Long id) {
        goodsRepository.deleteById(id);
    }

    /**
     * 通过搜索与过滤查询
     * @param request
     * @return
     */
    private QueryBuilder buildBasicQueryWithFilter(SearchRequest request) {
        //先构建基础查询
        BoolQueryBuilder queryBuilder = QueryBuilders.boolQuery();
        queryBuilder.must(QueryBuilders.matchQuery("all",request.getKey()).operator(Operator.AND));
        
        //然后构建过滤查询
        BoolQueryBuilder filterQueryBuilter = QueryBuilders.boolQuery();
        Map<String,String> filterMap = request.getFilter();
        for (Map.Entry<String,String> searchMap : filterMap.entrySet()) {
            String key = searchMap.getKey();
            String value = searchMap.getValue();
            if(key!="cid3"&&key!="brandId"){
                key = "specs."+key+".keyword";
            }
            filterQueryBuilter.must(QueryBuilders.matchQuery(key,value));
        }
        //基本查询添加过滤条件
        queryBuilder.filter(filterQueryBuilter);
        
        return queryBuilder;
    }

    /**
     * 聚合其它规格参数
     * @param
     * @param
     * @return
     */
    private List<Map<String, Object>> getSpecs(Category category, QueryBuilder basicQuery) {
        try {
            List<Map<String, Object>> specs = new ArrayList<>();
            //搜索出相应的规格参数
            List<SpecParam> specParams = specClient.querySpecParamByGid(null, category.getId(), true,null);
            //添加查询条件
            NativeSearchQueryBuilder queryBuilder = new NativeSearchQueryBuilder();
            queryBuilder.withQuery(basicQuery);

            for (SpecParam specParam: specParams) {
               String name = specParam.getName();
               //添加聚合
               queryBuilder.addAggregation(AggregationBuilders.terms(name).field("specs."+name+".keyword"));

            }
            //结果
            //AggregatedPage<Goods> result= template.queryForPage(queryBuilder.build(),Goods.class);
            AggregatedPage<Goods> result = (AggregatedPage<Goods>) goodsRepository.search(queryBuilder.build());
            //解析
            Aggregations aggs = result.getAggregations();

            for (SpecParam param : specParams) {
                //规格参数名称
                String name = param.getName();
                StringTerms terms = aggs.get(name);
                //StringTerms terms = (StringTerms) result.getAggregation(param.getName());
                //准备map
                Map<String,Object> map = new HashMap<>();
                map.put("k",name);
                map.put("options", terms.getBuckets()
                        .stream().map(bucket -> bucket.getKeyAsString()).collect(Collectors.toList()));
                specs.add(map);
            }

            return specs;
        }catch (Exception e){
            logger.error("规格参数聚合出现异常：", e);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 搜索分页与排序
     * @param queryBuilder
     * @param request
     */
    private void searchWithPageAndSort(NativeSearchQueryBuilder queryBuilder,SearchRequest request ){

        //排序
        String sortBy = request.getSortBy();
        Boolean descending = request.getDescending();
        if(StringUtils.isNotBlank(sortBy)){
            queryBuilder.withSort(SortBuilders.fieldSort(sortBy).order(descending ? SortOrder.DESC : SortOrder.ASC));
        }
    }

    /**
     * 获取商品聚合
     * @param aggregation
     * @return List<Category>
     */
    private List<Category> getCategoryAggResult(Aggregation aggregation) {
        try{
            List<Category> categories = new ArrayList<>();
            LongTerms longTerms= (LongTerms) aggregation;
            List<Long> cids = new ArrayList<>();
            for(LongTerms.Bucket bucket : longTerms.getBuckets()){
                cids.add(bucket.getKeyAsNumber().longValue());
            }
            List<Category> categoryList = categoryClient.queryNameByIds(cids);
            List<String> cnames = categoryList.stream().map(Category::getName).collect(Collectors.toList());
            for(int i = 0;i<cnames.size();i++){
                Category category = new Category();
                category.setId(cids.get(i));
                category.setName(cnames.get(i));
                categories.add(category);
            }
            return categories;
        }catch (Exception e){
            logger.error("商品聚合出现异常：", e);
            return null;
        }
    }
    /**
     * 获取品牌聚合
     * @param aggregation
     * @return List<Brand>
     */
    private List<Brand> getBrandAggResult(Aggregation aggregation) {
        try {
            LongTerms longTerms = (LongTerms) aggregation;
            List<Long> bids = longTerms.getBuckets().stream().map(bucket -> bucket.getKeyAsNumber().longValue()).collect(Collectors.toList());
            List<Brand> brands = brandClient.queryBrandByIds(bids);
            return brands;
        }catch (Exception e){
            logger.error("品牌聚合出现异常：", e);
            return null;
        }
    }


    /**
     * 把信息转换为goods对象形式
     * @param spu
     * @return
     */
    @Override
    public Goods buildsGoods(Spu spu){
        Long id = spu.getId();
        //查sku
        List<Sku> skus = goodsClient.querySkuListBySpuId(id);
        if(CollectionUtils.isEmpty(skus)){
            throw new LyException(ExceptionEnums.SKU_NOT_FOUND);
        }
        //查商品分类名称
        List<Category> categories = categoryClient.queryNameByIds(Arrays.asList(spu.getCid1(),spu.getCid2(),spu.getCid3()));
        if(CollectionUtils.isEmpty(categories)){
            throw new LyException(ExceptionEnums.CATEGORY_NOT_FOUND);
        }
        List<String> cname = categories.stream().map(Category::getName).collect(Collectors.toList());
        //查spudetail
        SpuDetail spuDetail = goodsClient.queryDetailById(id);
        //查规格参数
        List<SpecParam> specParams = specClient.querySpecParamByGid(null, spu.getCid3(), null, true);
        //处理sku数据
        List<Long> prices = new ArrayList<>();
        List<Map<String,Object>> skuList = new ArrayList<>();
        for (Sku sku:skus) {
            prices.add(sku.getPrice());
            Map<String,Object> map = new HashMap<>();
            //id,title,image,price
            map.put("id",sku.getId());
            map.put("title",sku.getTitle());
            //map.put("image", StringUtils.isBlank(sku.getImages())?"":sku.getImages().split(",")[0]);
            //substringBefore截取字符串分段的第一段
            map.put("image",StringUtils.substringBefore(sku.getImages(),","));
            map.put("price",sku.getPrice());
            skuList.add(map);
        }

        //处理规格参数
        Map<String,Object> specs = getSpecs(spu);

        //封装Goods
        Goods goods = new Goods();
        goods.setId(id);
        goods.setBrandId(spu.getBrandId());
        goods.setCid1(spu.getCid1());
        goods.setCid2(spu.getCid2());
        goods.setCid3(spu.getCid3());
        goods.setCreateTime(spu.getCreateTime());
        goods.setSubTitle(spu.getSubTitle());
        goods.setAll(spu.getTitle()+" "+StringUtils.join(cname," "));
        goods.setPrice(prices);
        goods.setSkus(JsonUtils.toString(skuList));
        goods.setSpecs(specs);

        return goods;
    }

    private HashMap<String, Object> getSpecs(Spu spu) {
        // 获取规格参数
        List<SpecParam> params = specClient.querySpecParamByGid(null, spu.getCid3(), true, null);
        if (CollectionUtils.isEmpty(params)) {
            throw new LyException(ExceptionEnums.SPECPARAM_NOT_FOUND);
        }
        // 查询商品详情
        SpuDetail spuDetail = goodsClient.queryDetailById(spu.getId());
        // 获取通用规格参数
        //获取通用规格参数
        Map<Long, String> genericSpec = JsonUtils.toMap(spuDetail.getGenericSpec(), Long.class, String.class);
        //获取特有规格参数
        Map<Long, List<String>> specialSpec = JsonUtils.nativeRead(spuDetail.getSpecialSpec(), new TypeReference<Map<Long, List<String>>>() {
        });

        //定义spec对应的map
        HashMap<String, Object> map = new HashMap<>();
        //对规格进行遍历，并封装spec，其中spec的key是规格参数的名称，值是商品详情中的值
        for (SpecParam param : params) {
            //key是规格参数的名称
            String key = param.getName();
            Object value = "";

            if (param.getGeneric()) {
                //参数是通用属性，通过规格参数的ID从商品详情存储的规格参数中查出值
                value = genericSpec.get(param.getId());
                if (param.getNumeric()) {
                    //参数是数值类型，处理成段，方便后期对数值类型进行范围过滤
                    value = chooseSegment(value.toString(), param);
                }
            } else {
                //参数不是通用类型
                value = specialSpec.get(param.getId());
            }
            value = (value == null ? "其他" : value);
            //存入map
            map.put(key, value);
        }
        return map;
    }


    //把数值类型的字符串，转换为一定分段范围的字符串
    private String chooseSegment(String value, SpecParam p) {
        double val = NumberUtils.toDouble(value);
        String result = "其它";
        // 保存数值段
        for (String segment : p.getSegments().split(",")) {
            String[] segs = segment.split("-");
            // 获取数值范围
            double begin = NumberUtils.toDouble(segs[0]);
            double end = Double.MAX_VALUE;
            if(segs.length == 2){
                end = NumberUtils.toDouble(segs[1]);
            }
            // 判断是否在范围内
            if(val >= begin && val < end){
                if(segs.length == 1){
                    result = segs[0] + p.getUnit() + "以上";
                }else if(begin == 0){
                    result = segs[1] + p.getUnit() + "以下";
                }else{
                    result = segment + p.getUnit();
                }
                break;
            }
        }
        return result;
    }

}
