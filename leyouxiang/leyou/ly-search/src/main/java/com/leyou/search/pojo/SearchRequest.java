package com.leyou.search.pojo;

import java.util.Map;

public class SearchRequest {
    private String key;  //定义关键字
    private Integer page; //定义当前页
    private String sortBy;
    private Boolean descending;
    private Map<String,String> filter ; //过滤项

    private static final Integer DEFAULT_SIZE = 20; //定义每页大小，this is default

    private static final Integer DEFAULT_PAGE = 1; //This is default page.

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getPage() {
        if(page==null){
            return DEFAULT_PAGE;
        }
        return Math.max(DEFAULT_PAGE,page);
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize(){
        return DEFAULT_SIZE;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public Boolean getDescending() {
        return descending;
    }

    public void setDescending(Boolean descending) {
        this.descending = descending;
    }

    public Map<String, String> getFilter() {
        return filter;
    }

    public void setFilter(Map<String, String> filter) {
        this.filter = filter;
    }
}
