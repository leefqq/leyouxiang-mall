package com.leyou.search.service;

import com.leyou.common.vo.PageResult;
import com.leyou.item.pojo.Spu;
import com.leyou.search.pojo.Goods;
import com.leyou.search.pojo.SearchRequest;


public interface SearchService {

    PageResult<Goods> search(SearchRequest result);

    void createIndex(Long id);

    void deleteIndex(Long id);

    Goods buildsGoods(Spu spu);
}
