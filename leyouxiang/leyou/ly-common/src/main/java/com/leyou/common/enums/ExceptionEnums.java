package com.leyou.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum ExceptionEnums {
    CATEGORY_NOT_FOUND(404,"商品分类没找到!"),
    BRAND_NOT_FOUND(404,"品牌信息没找到!"),
    SPECGROUP_NOT_FOUND(404,"规格参数组没找到!"),
    SPECPARAM_NOT_FOUND(404,"规格参数没找到!"),
    SPU_NOT_FOUND(404,"通用商品信息没找到!"),
    SPUDETAIL_NOT_FOUND(404,"通用商品信息详情没找到!"),
    SKU_NOT_FOUND(404,"该商品信息没找到!"),
    USER_NOT_FOUND(404,"该用户没找到!"),
    INVAILD_FILE_TYPE(500,"所选文件类型不正确!"),
    UPLOAD_FILE_ERROR(500,"上传文件失败！"),
    BRAND_SAVE_ERROR(500,"新增品牌失败！"),
    GOODS_SAVE_ERROR(500,"新增商品失败！"),
    CHECK_USERDATA_ERROR(500,"检查用户数据失败！"),
    SEND_VERIFY_CODE_ERROR(500,"发送验证码出错！"),
    REGISTER_ERROR(500,"用户注册出错！"),
    CODE_IS_NOT_RIGHT(500,"验证码不正确！"),
    PASSWORD_NOT_RIGHT(500,"密码不正确！"),
    LOGIN_ERROR(500,"登陆失败，密码或用户名不正确！"),
    CATEGORY_BRAND_SAVE_ERROR(500,"新增品牌分类中间表失败！"),
    CART_NOT_SKU(404,"购物车中无当前商品！"),
    ;
    private int code;
    private String msg;
}
