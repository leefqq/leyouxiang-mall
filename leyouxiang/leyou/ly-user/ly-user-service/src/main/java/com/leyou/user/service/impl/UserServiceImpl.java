package com.leyou.user.service.impl;


import com.leyou.common.enums.ExceptionEnums;
import com.leyou.common.exception.LyException;
import com.leyou.common.utils.CodecUtils;
import com.leyou.common.utils.NumberUtils;
import com.leyou.user.mapper.UserMapper;
import com.leyou.user.pojo.User;
import com.leyou.user.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private AmqpTemplate amqpTemplate;

    static final String KEY_PREFIX = "user:code:phone:";

    static final Logger logger = LoggerFactory.getLogger(UserService.class);

    /**
     * 校验数据是否可用
     * @param data
     * @param type
     * @return
     */
    @Override
    public Boolean checkUserData(String data, Integer type) {
        User check = new User();
        switch (type){
            case 1:
                check.setUsername(data);
                break;
            case 2:
                check.setPhone(data);
                break;
            default:
                return null;

        }
        Boolean boo = userMapper.selectCount(check)==0;
        if(boo==null){
            throw new LyException(ExceptionEnums.CHECK_USERDATA_ERROR);
        }
        return boo;
    }

    /**
     * 发送验证码
     * @param phone
     */
    @Override
    public void sendVerifyCode(String phone) {
        //创建随机码
        String code = NumberUtils.generateCode(6);
        try {
            //发送随机码到sms服务
            Map<String,String> msg = new HashMap<>();
            msg.put("phone",phone);
            msg.put("code",code);
            amqpTemplate.convertAndSend("ly.sms.exchange","sms.verify.code",msg);
            //存入redis中，设置有效期
            redisTemplate.opsForValue().set(KEY_PREFIX+phone,code,5, TimeUnit.MINUTES);
        }catch (Exception e){
            logger.error("发送验证码失败！"+e);
            throw new LyException(ExceptionEnums.SEND_VERIFY_CODE_ERROR);
        }
    }

    @Override
    public void register(User user, String code) {
        //从redis中校验短信码
        String key = KEY_PREFIX+user.getPhone();
        String codeHash = redisTemplate.opsForValue().get(key);
        if(!code.equals(codeHash)){
            throw new LyException(ExceptionEnums.CODE_IS_NOT_RIGHT);
        }
        //生成密码盐
        String salt = CodecUtils.generateSalt();
        user.setSalt(salt);

        user.setCreated(new Date());
        //对密码进行MD5加密
        String codePassword = CodecUtils.md5Hex(user.getPassword(),salt);
        user.setPassword(codePassword);
        //写入数据库
        Boolean boo = userMapper.insert(user)==1;
        //注册成功删除key
        if(boo&&boo!=null){
            redisTemplate.delete(key);
        }else {
            logger.error("删除缓存值失败！key:{}",key);
            throw new LyException(ExceptionEnums.REGISTER_ERROR);
        }

    }

    @Override
    public User queryUser(String username, String password) {
        User user = new User();
        user.setUsername(username);
        User one = userMapper.selectOne(user);
        if (one==null){
            throw new LyException(ExceptionEnums.USER_NOT_FOUND);
        }
        if(!one.getPassword().equals(CodecUtils.md5Hex(password,one.getSalt()))){
            throw new LyException(ExceptionEnums.PASSWORD_NOT_RIGHT);
        }
        return one;
    }
}
