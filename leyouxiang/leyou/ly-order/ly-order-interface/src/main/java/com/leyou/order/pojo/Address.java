package com.leyou.order.pojo;

import lombok.Data;

@Data
public class Address {
    private String id;
    private String name;
    private String phone;
    private String state; //省
    private String city; //市
    private String district; //区/县
    private String zipCode; //邮编
    private String address; //详细地址
    private String label; //地址标签
    private Boolean defaultAddress; //是否为默认地址
}
