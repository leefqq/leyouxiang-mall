package com.leyou.order.service;

import com.leyou.order.pojo.Address;

import java.util.List;

public interface AddressService {
    List<Address> queryAddressList();

    void addressSave(Address address);

    Address queryAddress(String id);

    void addressUpdate(Address address);
}
