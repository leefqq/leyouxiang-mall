package com.leyou.order.web;

import com.leyou.order.pojo.Address;
import com.leyou.order.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    /**
     *
     * @param id
     * @return
     */
    @RequestMapping("/{id}")
    public ResponseEntity<Address> addressManage(@PathVariable(value = "id",required = true)String id){

        return ResponseEntity.ok(addressService.queryAddress(id));
    }

    /**
     * 查询地址列表
     * @return
     */
    @RequestMapping("/list")
    public ResponseEntity<List<Address>> queryAddressList(){
        return ResponseEntity.ok(addressService.queryAddressList());
    }

    @PostMapping("/save")
    public ResponseEntity<Void> addressSave(@RequestBody Address address){
        addressService.addressSave(address);

        return ResponseEntity.ok().build();
    }

    @PutMapping("/update")
    public ResponseEntity<Void> addressUpdate(@RequestBody Address address) {
        addressService.addressUpdate(address);

        return ResponseEntity.ok().build();
    }
}
