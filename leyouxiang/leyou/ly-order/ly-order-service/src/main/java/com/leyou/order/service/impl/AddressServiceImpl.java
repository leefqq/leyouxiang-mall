package com.leyou.order.service.impl;

import com.leyou.auth.entiy.UserInfo;
import com.leyou.cart.filter.LoginInterceptor;
import com.leyou.common.utils.JsonUtils;
import com.leyou.common.utils.NumberUtils;
import com.leyou.common.utils.UUIDUtil;
import com.leyou.order.pojo.Address;
import com.leyou.order.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class AddressServiceImpl implements AddressService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    private static final String ADD_KEY = "ly:address:uid:";

    /**
     * 查询地址列表
     * @return
     */
    @Override
    public List<Address> queryAddressList() {
        UserInfo userInfo = LoginInterceptor.getLoginUser();
        String key = getAddKey();
        if(!redisTemplate.hasKey(key)){
            log.error("{}无可用地址！id:{}",userInfo.getUsername(),userInfo.getId());
            return null;
        }
        BoundHashOperations<String, Object, Object> addHashOps = redisTemplate.boundHashOps(key);

        List<Object> addresses = addHashOps.values();
        if(CollectionUtils.isEmpty(addresses)){
            log.error("{}有key但是没有可用地址！id:{}",userInfo.getUsername(),userInfo.getId());
            return null;
        }
        List<Address> addressList = addresses.stream().map(a ->
                JsonUtils.toBean(a.toString(),Address.class)
        ).collect(Collectors.toList());

        return addressList;
    }

    /**
     * 保存地址
     * @param address
     */
    @Override
    public void addressSave(Address address) {
        String key = getAddKey();
        BoundHashOperations<String, Object, Object> addBhash = redisTemplate.boundHashOps(key);
        //补充属性
        address.setId(UUIDUtil.uuid());
        addBhash.put(address.getId().toString(),JsonUtils.toString(address));
    }

    @Override
    public Address queryAddress(String id) {
        String key = getAddKey();
        BoundHashOperations<String, Object, Object> addBhash = redisTemplate.boundHashOps(key);
        String sAddress = addBhash.get(id).toString();
        Address address = JsonUtils.toBean(sAddress, Address.class);
        if(address == null){
            log.error("地址为空");
            return null;
        }
        return address;
    }

    /**
     * 修改地址
     * @param address
     */
    @Override
    public void addressUpdate(Address address) {
        String key = getAddKey();
        BoundHashOperations<String, Object, Object> addbHash = redisTemplate.boundHashOps(key);
        String sAddress = addbHash.get(address.getId()).toString();
        Address oldAddress = JsonUtils.toBean(sAddress, Address.class);
        BeanUtils.copyProperties(address,oldAddress);
        addbHash.put(oldAddress.getId(),JsonUtils.toString(oldAddress));

    }


    private String getAddKey(){
        UserInfo userInfo = LoginInterceptor.getLoginUser();
        String key = ADD_KEY+userInfo.getId();
        return key;
    }
}
