package com.leyou.item.service;

import com.leyou.item.pojo.SpecGroup;
import com.leyou.item.pojo.SpecParam;

import java.util.List;

public interface SpecService {
    List<SpecGroup> querySpecGroupsByCid(Long cid);

    /**
     * 通过参数组id查询具体参数
     * @param gid
     * @return
     */
    List<SpecParam> querySpecParamByGid(Long gid,Long cid,Boolean searching,Boolean generic);

    /**
     * 通过cid查询拼装的spec
     * @param id
     * @return
     */
    List<SpecGroup> querySpecsByCid(Long id);
}
