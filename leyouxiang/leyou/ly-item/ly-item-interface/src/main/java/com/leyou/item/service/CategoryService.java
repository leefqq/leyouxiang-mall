package com.leyou.item.service;

import com.leyou.item.pojo.Category;

import java.util.List;

public interface CategoryService {
    List<Category> queryCategoryListByParentId(Long pid);

    /**
     * 通过品牌id查询商品分类
     * @param bid
     * @return
     */
    List<Category> queryByBrandId(Long bid);

    /**
     * 通过商品分类id查询商品分类名字
     * @param ids
     * @return
     */
    List<String> queryNameByIds(List<Long> ids);

    /**
     * 根据商品分类id查询商品分类
     * @param ids
     * @return
     */
    List<Category> queryCategoryByIds(List<Long> ids);

    List<Category> queryAllByCid3(Long id);
}
