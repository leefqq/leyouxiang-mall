package com.leyou.item.service;

import com.leyou.common.vo.PageResult;
import com.leyou.item.pojo.Sku;
import com.leyou.item.pojo.Spu;
import com.leyou.item.pojo.SpuDetail;
import com.leyou.item.pojo.SpuParam;
import com.leyou.item.vo.SpuVo;

import java.util.List;

public interface GoodsService {
    PageResult<SpuVo> querySpuPage(SpuParam spuParam);

    void saveGoods(SpuVo spu);

    SpuDetail querySpuDetailById(Long id);

    List<Sku> querySkuListBySpuId(Long spuId);

    void updateGoods(SpuVo spuVo);

    Spu querySpuById(Long id);

    Sku querySkuById(Long skuId);
}
