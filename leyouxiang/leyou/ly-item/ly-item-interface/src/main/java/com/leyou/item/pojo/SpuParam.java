package com.leyou.item.pojo;

import lombok.Data;

@Data
public class SpuParam {
    public String key;
    public Boolean saleable;
    public Integer page;
    public Integer rows;
}
