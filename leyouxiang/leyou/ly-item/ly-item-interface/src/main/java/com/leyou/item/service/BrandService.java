package com.leyou.item.service;

import com.leyou.common.vo.PageResult;
import com.leyou.item.pojo.Brand;
import com.leyou.item.pojo.PageParam;

import java.util.List;

public interface BrandService {
    /**
     * 通过分页结果查询品牌列表
     * @param pageParam
     * @return
     */
    PageResult<Brand> queryBrandByPage(PageParam pageParam);

    /**
     * 新增品牌
     * @param brand
     * @param cids
     */
    void saveBrand(Brand brand, List<Long> cids);

    /**
     * 根据商品分类查询品牌信息
     * @param cid
     * @return
     */
    List<Brand> queryBrandByCid(Long cid);

    /**
     * 根据多个id查询商品列表
     * @param ids
     * @return
     */
    List<Brand> queryBrandByIds(List<Long> ids);
}
