package com.leyou.item.pojo;

import lombok.Data;

@Data
public class PageParam {
    public String key;
    public Integer page;
    public Integer rows;
    public String sortBy;
    public Boolean desc;
}
