package com.leyou.item.web;

import com.leyou.common.vo.PageResult;
import com.leyou.item.pojo.Sku;
import com.leyou.item.pojo.Spu;
import com.leyou.item.pojo.SpuDetail;
import com.leyou.item.pojo.SpuParam;
import com.leyou.item.service.GoodsService;
import com.leyou.item.vo.SpuVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    @GetMapping("spu/page")
    public ResponseEntity<PageResult<SpuVo>> querySpuPage(/*SpuParam spuParam*/
            @RequestParam(value = "page", defaultValue = "1") Integer page,
            @RequestParam(value = "rows", defaultValue = "5") Integer rows,
            @RequestParam(value = "saleable", required = false) Boolean saleable,
            @RequestParam(value = "key", required = false) String key){
        SpuParam spuParam = new SpuParam();
        spuParam.setPage(page);
        spuParam.setRows(rows);
        spuParam.setSaleable(saleable);
        spuParam.setKey(key);
        if(spuParam.getPage()==null){
            spuParam.setPage(1);
        }
        if(spuParam.getRows()==null){
            spuParam.setRows(5);
        }
        return ResponseEntity.ok(goodsService.querySpuPage(spuParam));
    }

    @PostMapping("goods")
    public ResponseEntity<Void> saveGoods(@RequestBody SpuVo spuVo){
        goodsService.saveGoods(spuVo);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 根据spuid查询商品详情
     * @param spuId
     * @return
     */
    @GetMapping("spu/detail/{id}")
    public ResponseEntity<SpuDetail> queryDetailById(@PathVariable("id")Long spuId){
        return ResponseEntity.ok(goodsService.querySpuDetailById(spuId));
    }

    /**
     * 根据spuid查询sku
     * @param spuId
     * @return
     */
    @GetMapping("sku/list")
    public ResponseEntity<List<Sku>> querySkuListBySpuId(@RequestParam("id")Long spuId){
        return ResponseEntity.ok(goodsService.querySkuListBySpuId(spuId));
    }

    @PutMapping("goods")
    public ResponseEntity<Void> updateGoods(@RequestBody SpuVo spuVo){
        goodsService.updateGoods(spuVo);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 根据spuid查询spu
     * @param id
     * @return
     */
    @GetMapping("spu/{id}")
    public ResponseEntity<Spu> querySpuById(@PathVariable("id") Long id){
        Spu spu = this.goodsService.querySpuById(id);
        return ResponseEntity.ok(spu);
    }

    @GetMapping("sku/{id}")
    public ResponseEntity<Sku> querySkuById(@PathVariable("id")Long skuId){
        Sku sku = goodsService.querySkuById(skuId);
        return ResponseEntity.ok(sku);
    }
}
