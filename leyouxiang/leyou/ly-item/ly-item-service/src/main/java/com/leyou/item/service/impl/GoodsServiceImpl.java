package com.leyou.item.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.leyou.common.enums.ExceptionEnums;
import com.leyou.common.exception.LyException;
import com.leyou.common.vo.PageResult;
import com.leyou.item.mapper.*;
import com.leyou.item.pojo.*;
import com.leyou.item.service.CategoryService;
import com.leyou.item.service.GoodsService;
import com.leyou.item.vo.SpuVo;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private SpuMapper spuMapper;
    @Autowired
    private SpuDetailMapper spuDetailMapper;
    @Autowired
    private SkuMapper skuMapper;
    @Autowired
    private StockMapper stockMapper;
    @Autowired
    private BrandMapper brandMapper;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private AmqpTemplate amqpTemplate;

    private static final Logger logger = LoggerFactory.getLogger(GoodsService.class);


    @Transactional
    @Override
    public PageResult<SpuVo> querySpuPage(SpuParam spuParam) {
        //分页
        PageHelper.startPage(spuParam.getPage(),spuParam.getRows());
        //过滤
        Example example = new Example(Spu.class);
        Example.Criteria criteria = example.createCriteria();
        String key = spuParam.getKey();
        //上下架
        if(spuParam.getSaleable()!=null){
            criteria.orEqualTo("saleable",spuParam.getSaleable());
        }
        //Example.Criteria criteria = example.createCriteria();
        if(StringUtils.isNotBlank(key)){
            criteria.andLike("title", "%" + key + "%");
        }
        //默认排序
        example.setOrderByClause("last_update_time DESC");
        //查询
        List<Spu> spuList = spuMapper.selectByExample(example);
        Page<Spu> spus = (Page<Spu>)spuList;
        if (CollectionUtils.isEmpty(spus)) {
            throw new LyException(ExceptionEnums.SPU_NOT_FOUND);
        }
        //解析分类信息和品牌名称
        //loadCategoryAndBrand(spus);
        //分页结果
        PageInfo<Spu> pageInfo = new PageInfo<>(spus);

        //转换，解析分类信息和品牌名称
       List<SpuVo> spuVoList = spus.getResult().stream().map(spu->{
           //转换spu为spuvo
            SpuVo spuVo = new SpuVo();
           //属性拷贝
            BeanUtils.copyProperties(spu,spuVo);
           //查询spu的商品分类名称，要查三级分类
            List<String> names = categoryService.queryNameByIds(
                    Arrays.asList(spu.getCid1(),spu.getCid2(),spu.getCid3())
            );
            //商品分类，拼接成 ../../..的形式，存入
            spuVo.setCname(StringUtils.join(names,"/"));
            //查询品牌名称
            Brand brand = brandMapper.selectByPrimaryKey(spu.getBrandId());
            spuVo.setBname(brand.getName());
            return spuVo;
        }).collect(Collectors.toList());
        return new PageResult<>(pageInfo.getTotal(),spuVoList);
    }

    /**
     * 根据Spuvo存储商品
     * @param spu
     */
    @Override
    @Transactional
    public void saveGoods(SpuVo spu) {
        //首先新增Spu表
        spu.setSaleable(true);
        spu.setValid(true);
        spu.setCreateTime(new Date());
        spu.setLastUpdateTime(spu.getCreateTime());
        int Spu_i = spuMapper.insert(spu);
        if(Spu_i!=1){
            throw new LyException(ExceptionEnums.GOODS_SAVE_ERROR);
        }
        //然后新增spu_detail表
        spu.getSpuDetail().setSpuId(spu.getId());
        int Spud_i = spuDetailMapper.insert(spu.getSpuDetail());
        if(Spud_i!=1){
            throw new LyException(ExceptionEnums.GOODS_SAVE_ERROR);
        }
        //新增sku和stock表
        saveSkuAndStock(spu.getSkus(),spu.getId());
        //发送消息
        sendMessage(spu.getId(),"insert");
    }

    /**
     * 保存sku和库存表
     * @param skus
     * @param spuId
     */
    private void saveSkuAndStock(List<Sku> skus, Long spuId) {
        for (Sku sku:
             skus) {
            if(!sku.getEnable()){
                continue;
            }
            sku.setSpuId(spuId);
            sku.setCreateTime(new Date());
            sku.setLastUpdateTime(sku.getCreateTime());
            int Sku_i = skuMapper.insert(sku);
            if(Sku_i!=1){
                throw new LyException(ExceptionEnums.GOODS_SAVE_ERROR);
            }
            Stock stock = new Stock();
            stock.setSkuId(sku.getId());
            stock.setStock(sku.getStock());
            int Sto_i = stockMapper.insert(stock);
            if(Sto_i!=1){
                throw new LyException(ExceptionEnums.GOODS_SAVE_ERROR);
            }
        }
    }

    /**
     * 根据id查询spu商品
     * @param id
     * @return
     */
    @Override
    public SpuDetail querySpuDetailById(Long id) {
        SpuDetail spuDetail = spuDetailMapper.selectByPrimaryKey(id);
        if(spuDetail==null){
            throw new LyException(ExceptionEnums.SPUDETAIL_NOT_FOUND);
        }

        return spuDetail;
    }

    /**
     * 根据spu_id查询sku列表
     * @param spuId
     * @return
     */
    @Override
    public List<Sku> querySkuListBySpuId(Long spuId) {
        Sku sku = new Sku();
        sku.setSpuId(spuId);
        List<Sku> skus = skuMapper.select(sku);
        if(CollectionUtils.isEmpty(skus)){
            throw new LyException(ExceptionEnums.SKU_NOT_FOUND);
        }
        return skus;
    }

    /**
     * 修改商品
     * @param spuVo
     */
    @Transactional
    @Override
    public void updateGoods(SpuVo spuVo) {
    //先删除sku和库存
        //先查询sku
        List<Sku> skus = querySkuListBySpuId(spuVo.getId());
        //如果以前存在，则删除库存
        if(!CollectionUtils.isEmpty(skus)){
            //删除stock
            List<Long> ids = skus.stream().map(s->s.getId()).collect(Collectors.toList());
            Example example = new Example(Stock.class);
            example.createCriteria().andIn("skuId",ids);
            stockMapper.deleteByExample(example);
            //删除sku
            Sku oSku = new Sku();
            oSku.setSpuId(spuVo.getId());
            skuMapper.delete(oSku);
        }
        //然后新插入sku和库存
        saveSkuAndStock(spuVo.getSkus(),spuVo.getId());
        //后更新spu表和spu_detail数据
        spuVo.setLastUpdateTime(new Date());
        spuVo.setCreateTime(null);
        spuVo.setSaleable(null);
        spuVo.setValid(null);
        spuMapper.updateByPrimaryKeySelective(spuVo);
        spuDetailMapper.updateByPrimaryKeySelective(spuVo.getSpuDetail());

        sendMessage(spuVo.getId(),"update");
    }

    /**
     * 通过spuid查询spu
     * @param id
     * @return
     */
    @Override
    public Spu querySpuById(Long id) {
        Spu spu = spuMapper.selectByPrimaryKey(id);
        if(spu ==null){
            throw new LyException(ExceptionEnums.SPU_NOT_FOUND);
        }
        return spu;
    }

    @Override
    public Sku querySkuById(Long skuId) {
        Sku sku = skuMapper.selectByPrimaryKey(skuId);
        if(sku == null){
            throw new LyException(ExceptionEnums.SKU_NOT_FOUND);
        }
        return sku;
    }

    /**
     * 修改商品信息的时候发送消息
     * @param id
     * @param type
     */
    private void sendMessage(Long id, String type){
        // 发送消息
        try {
            this.amqpTemplate.convertAndSend("item." + type, id);
        } catch (Exception e) {
            logger.error("{}商品消息发送异常，商品id：{}", type, id, e);
        }
    }

}
