package com.leyou.item.web;

import com.leyou.common.vo.PageResult;
import com.leyou.item.pojo.Brand;
import com.leyou.item.pojo.PageParam;
import com.leyou.item.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    /**
     * 分页查询品牌信息
     * @param pageParam
     * @return
     */
    @PostMapping("page")
    public ResponseEntity<PageResult<Brand>> queryBrandByPage(@RequestBody PageParam pageParam){
        if(pageParam.getPage()==null){
            pageParam.setPage(1);
        }
        if(pageParam.getRows()==null){
            pageParam.setRows(5);
        }
        if(pageParam.getDesc()==null){
            pageParam.setDesc(false);
        }
        PageResult<Brand> page = brandService.queryBrandByPage(pageParam);
        return ResponseEntity.ok(page);
    }

    @PostMapping
    public ResponseEntity<Void> saveBrand(Brand brand,@RequestParam("cids") List<Long> cids){
        brandService.saveBrand(brand,cids);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    /**
     * 通过cid查询品牌列表
     * @param cid
     * @return
     */
    @GetMapping("cid/{cid}")
    public ResponseEntity<List<Brand>> queryBrandByCid(@PathVariable("cid") Long cid){
        return ResponseEntity.ok(brandService.queryBrandByCid(cid));
    }

    @GetMapping("list")
    public ResponseEntity<List<Brand>> queryBrandByIds(@RequestParam("ids")List<Long> ids){

        return ResponseEntity.ok(brandService.queryBrandByIds(ids));
    }

}
