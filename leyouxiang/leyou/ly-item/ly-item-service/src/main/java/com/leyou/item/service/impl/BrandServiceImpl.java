package com.leyou.item.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.leyou.common.enums.ExceptionEnums;
import com.leyou.common.exception.LyException;
import com.leyou.common.vo.PageResult;
import com.leyou.item.mapper.BrandMapper;
import com.leyou.item.pojo.Brand;
import com.leyou.item.pojo.PageParam;
import com.leyou.item.service.BrandService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandMapper brandMapper;

    /**
     * 通过分页结果查询品牌列表
     * @param pageParam
     * @return
     */
    @Override
    public PageResult<Brand> queryBrandByPage(PageParam pageParam) {
        //分页
        PageHelper.startPage(pageParam.getPage(),pageParam.getRows());
        //过滤
        Example example = new Example(Brand.class);
        if(StringUtils.isNotBlank(pageParam.getKey())){
            example.createCriteria().andLike("name","%"+pageParam.getKey()+"%")
                    .orEqualTo("letter",pageParam.getKey());
        }
        //排序
        if(StringUtils.isNotBlank(pageParam.getSortBy())){
            String orderByClause = pageParam.getSortBy()+(pageParam.getDesc() ? " DESC":" ASC");
            example.setOrderByClause(orderByClause);
        }
        //查询
        List<Brand> brands = brandMapper.selectByExample(example);
        if(CollectionUtils.isEmpty(brands)){
            throw new LyException(ExceptionEnums.BRAND_NOT_FOUND);
        }
        PageInfo<Brand> pageInfo = new PageInfo<>(brands);
        return new PageResult<>(pageInfo.getTotal(),brands);
    }

    /**
     * 增加品牌信息
     * @param brand
     * @param cids
     */
    @Override
    public void saveBrand(Brand brand, List<Long> cids) {
        //设置新增品牌的id
        brand.setId(null);
        int insert = brandMapper.insert(brand);
        if(insert!=1){
            throw new LyException(ExceptionEnums.BRAND_SAVE_ERROR);
        }
        //新增中间表
        for(Long cid:cids){
            int count = brandMapper.insertCategoryBrand(cid, brand.getId());
            if(count != 1){
                throw new LyException(ExceptionEnums.CATEGORY_BRAND_SAVE_ERROR);
            }
        }
    }
    /**
     * 根据商品分类查询品牌信息
     * @param cid
     * @return
     */
    @Override
    public List<Brand> queryBrandByCid(Long cid) {

        List<Brand> brands = brandMapper.queryBrandByCid(cid);
        if (CollectionUtils.isEmpty(brands)) {
            throw new LyException(ExceptionEnums.BRAND_NOT_FOUND);
        }
        return brands;
    }

    @Override
    public List<Brand> queryBrandByIds(List<Long> ids) {
        List<Brand> brands = brandMapper.selectByIdList(ids);
        if(CollectionUtils.isEmpty(brands)){
            throw new LyException(ExceptionEnums.BRAND_NOT_FOUND);
        }
        return brands;
    }

}
