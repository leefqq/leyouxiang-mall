package com.leyou.item.web;

import com.leyou.item.pojo.SpecGroup;
import com.leyou.item.pojo.SpecParam;
import com.leyou.item.service.SpecService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("spec")
public class SpecController {
    @Autowired
    private SpecService specService;

    @GetMapping("groups/{cid}")
    public ResponseEntity<List<SpecGroup>> querySpecGroupsByCid(@PathVariable("cid") Long cid){

        return ResponseEntity.ok(specService.querySpecGroupsByCid(cid));
    }


    @GetMapping("params")
    public ResponseEntity<List<SpecParam>> querySpecParamByGid(@RequestParam(value = "gid",required = false)Long gid,
                                                               @RequestParam(value = "cid",required = false)Long cid,
                                                               @RequestParam(value = "searching",required = false)Boolean searching,
                                                               @RequestParam(value = "generic",required = false)Boolean generic){

        return ResponseEntity.ok(specService.querySpecParamByGid(gid,cid,searching,generic));
    }

    @GetMapping("{cid}")
    public ResponseEntity<List<SpecGroup>> querySpecGroupByCid(@PathVariable("cid") Long cid){

        return ResponseEntity.ok(specService.querySpecGroupsByCid(cid));
    }

}
