package com.leyou.item.service.impl;

import com.leyou.common.enums.ExceptionEnums;
import com.leyou.common.exception.LyException;
import com.leyou.item.mapper.SpecGroupMapper;
import com.leyou.item.mapper.SpecParamMapper;
import com.leyou.item.pojo.SpecGroup;
import com.leyou.item.pojo.SpecParam;
import com.leyou.item.service.SpecService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class SpecServiceImpl implements SpecService {
    @Autowired
    private SpecGroupMapper specGroupMapper;
    @Autowired
    private SpecParamMapper specParamMapper;

    @Override
    public List<SpecGroup> querySpecGroupsByCid(Long cid) {
        SpecGroup specGroup = new SpecGroup();
        specGroup.setCid(cid);
        List<SpecGroup> specGroups = specGroupMapper.select(specGroup);
        if(CollectionUtils.isEmpty(specGroups)){
            throw new LyException(ExceptionEnums.SPECGROUP_NOT_FOUND);
        }
        return specGroups;
    }

    /**
     * 通过参数组查询具体参数
     * @param gid
     * @return
     */
    @Override
    public List<SpecParam> querySpecParamByGid(Long gid,Long cid,Boolean searching,Boolean generic) {
        SpecParam specParam = new SpecParam();
        specParam.setGroupId(gid);
        specParam.setCid(cid);
        specParam.setSearching(searching);
        specParam.setGeneric(generic);
        List<SpecParam> specParams = specParamMapper.select(specParam);
        if (CollectionUtils.isEmpty(specParams)) {
            throw new LyException(ExceptionEnums.SPECPARAM_NOT_FOUND);
        }
        return specParams;
    }

    @Override
    public List<SpecGroup> querySpecsByCid(Long id) {
        List<SpecGroup> specGroups = querySpecGroupsByCid(id);
        if(CollectionUtils.isEmpty(specGroups)){
            throw new LyException(ExceptionEnums.SPECGROUP_NOT_FOUND);
        }
        specGroups.forEach(g->{
            g.setParams(querySpecParamByGid(g.getId(),null,null,null));
        });
        return specGroups;
    }
}
