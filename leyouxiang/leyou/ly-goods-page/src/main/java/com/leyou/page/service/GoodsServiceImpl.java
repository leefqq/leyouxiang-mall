package com.leyou.page.service;

import com.leyou.common.enums.ExceptionEnums;
import com.leyou.common.exception.LyException;
import com.leyou.item.pojo.*;
import com.leyou.page.feignclient.BrandClient;
import com.leyou.page.feignclient.CategoryClient;
import com.leyou.page.feignclient.GoodsClient;
import com.leyou.page.feignclient.SpecClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GoodsServiceImpl implements GoodsService{
    @Autowired
    private GoodsClient goodsClient;
    @Autowired
    private SpecClient specClient;
    @Autowired
    private CategoryClient categoryClient;
    @Autowired
    private BrandClient brandClient;

    private static final Logger logger = LoggerFactory.getLogger(GoodsService.class);

    @Override
    public Map<String,Object> loadModel(Long id){
        try {
            //创建模型Map
            Map<String,Object> modelMap = new HashMap<>();
            //查询各类数据：spu、spudetail、skus、category、specparam、brands、specgroups
            Spu spu = goodsClient.querySpuById(id);
            SpuDetail spuDetail = goodsClient.queryDetailById(id);
            List<Sku> skus = goodsClient.querySkuListBySpuId(id);


            List<Category> categories = getCategories(spu);
            if(CollectionUtils.isEmpty(categories)){
                logger.error("无商品分类信息封装");
            }


            List<Brand> brands = brandClient.queryBrandByIds(Arrays.asList(spu.getBrandId()));

            List<SpecGroup> specGroups = specClient.querySpecGroupByCid(spu.getCid3());

            //封装specGroup的属性
            for(SpecGroup specGroup : specGroups){
                List<SpecParam> specParams = specClient.querySpecParamByGid(specGroup.getId(),specGroup.getCid(),null,null);
                if(!CollectionUtils.isEmpty(specParams)){
                    specGroup.setParams(specParams);
                }
            }


            //specparam封装成id：name的键值对形式
            List<SpecParam> specParams = specClient.querySpecParamByGid(null, spu.getCid3(), null, false);
            Map<Long,String> specMap = new HashMap<>();
            for (SpecParam specParam : specParams) {
                specMap.put(specParam.getId(),specParam.getName());
            }
            modelMap.put("spu",spu);
            modelMap.put("spuDetail",spuDetail);
            modelMap.put("skus",skus);
            modelMap.put("brand",brands.get(0));
            modelMap.put("categories",categories);
            modelMap.put("specs",specGroups);
            modelMap.put("paramMap",specMap);

            return modelMap;
        }catch (Exception e){
            logger.error("封装模型数据出错,spuId:{}",id,e);
        }
        return null;
    }

    private List<Category> getCategories(Spu spu) {
        List<Category> categories = categoryClient.queryNameByIds(
                Arrays.asList(spu.getCid1(), spu.getCid2(), spu.getCid3()));
        Category c1 = new Category();
        c1.setId(spu.getCid1());
        c1.setName(categories.get(0).getName());
        Category c2 = new Category();
        c2.setId(spu.getCid2());
        c2.setName(categories.get(1).getName());
        Category c3 = new Category();
        c3.setId(spu.getCid3());
        c3.setName(categories.get(2).getName());

        return Arrays.asList(c1,c2,c3);
    }


}
