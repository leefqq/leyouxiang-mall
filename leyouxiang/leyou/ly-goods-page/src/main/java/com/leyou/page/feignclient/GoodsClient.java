package com.leyou.page.feignclient;


import com.leyou.item.api.GoodsApi;
import org.springframework.cloud.openfeign.FeignClient;

import java.util.Map;


@FeignClient("item-service")
public interface GoodsClient extends GoodsApi {

}
