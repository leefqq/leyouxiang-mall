package com.leyou.upload.service;

import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.leyou.common.enums.ExceptionEnums;
import com.leyou.common.exception.LyException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class UploadServiceImpl implements UploadService{
    private static final List<String> allowType = Arrays.asList("image/png","image/jpeg"
    ,"image/bmp");

    @Autowired
    private FastFileStorageClient storageClient;

    @Override
    public String uploadImage(MultipartFile file) {
        try {
            //校验文件类型
            String contengType = file.getContentType();
            if(!allowType.contains(contengType)){
                throw new LyException(ExceptionEnums.INVAILD_FILE_TYPE);
            }
            //校验图片内容
            BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
            if(bufferedImage == null){
                throw new LyException(ExceptionEnums.INVAILD_FILE_TYPE);
            }
            //上传图片到FastDfs
            //获取图片后缀
            String s = StringUtils.substringAfterLast(file.getOriginalFilename(), ".");
            //上传
            StorePath storePath = storageClient.uploadFile(file.getInputStream(), file.getSize(), s, null);
            //拼接图片地址
            String url = "http://image.leyou.com/"+storePath.getFullPath();
            return url;
        }catch (IOException e){
            log.error("上传文件失败！"+e);
            throw new LyException(ExceptionEnums.UPLOAD_FILE_ERROR);
        }
    }
}
