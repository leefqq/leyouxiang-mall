package com.leyou.sms.listener;

import com.leyou.sms.service.SmsService;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class SmsListener {
    @Autowired
    private SmsService smsService;
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = "sms.code.send.queue",durable = "true"),
            exchange = @Exchange(
                    value = "ly.sms.exchange",
                    type = ExchangeTypes.TOPIC),
            key = {"sms.verify.code"}))
    public void listenSendCode(Map<String,String> msg){
        if(msg==null){
            return;
        }
        try {
            smsService.sendCode(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
