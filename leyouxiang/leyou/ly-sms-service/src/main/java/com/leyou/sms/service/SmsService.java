package com.leyou.sms.service;

import org.springframework.stereotype.Service;


import java.io.FileWriter;
import java.util.Date;
import java.util.Map;

@Service
public class SmsService {
    public void sendCode(Map<String, String> msg) throws Exception{
        String code = msg.get("code");
        FileWriter fw = new FileWriter("code.txt",true);
        fw.write("您的验证码是："+code+" 时间："+new Date()+"有效期为5分钟。\n");
        fw.close();

    }
}
