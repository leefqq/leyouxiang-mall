package com.leyou.gateway.filter;

import com.leyou.auth.entiy.UserInfo;
import com.leyou.auth.utils.JwtUtils;
import com.leyou.common.utils.CookieUtils;
import com.leyou.gateway.config.FilterProperties;
import com.leyou.gateway.config.JwtProperties;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Component
@EnableConfigurationProperties({JwtProperties.class, FilterProperties.class})
public class AuthFilter extends ZuulFilter {

    @Autowired
    private JwtProperties prop;

    @Autowired
    private FilterProperties filterProperties;

    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;  //类型
    }

    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER - 1; //过滤器顺序
    }

    /**
     * 拦截内容
     * @return
     */
    @Override
    public boolean shouldFilter() {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String requestURI = request.getRequestURI();
        boolean isAllow = isAllowURI(requestURI);
        return !isAllow;
    }

    /**
     * 判断是否为指定前缀URI
     * @param requestURI
     * @return
     */
    private boolean isAllowURI(String requestURI) {
        for(String allowURI : filterProperties.getAllowPaths()){
            if(requestURI.startsWith(allowURI)){
                return true;
            }
        }
        return false;
    }


    @Override
    public Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();
        String token = CookieUtils.getCookieValue(request,prop.getCookieName());
        try{
            UserInfo userInfo = JwtUtils.getInfoFromToken(token,prop.getPublicKey());

        }catch (Exception e){
            ctx.setSendZuulResponse(false); //拦截
            ctx.setResponseStatusCode(403); //返回状态码
        }
        return null;
    }
}
